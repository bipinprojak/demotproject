#!/bin/sh

# -----------------------------------------------------------------------
# JRE location -- edit or remove (if already set) for your environment
# -----------------------------------------------------------------------

JAVA_HOME="/opt/jdk1.5.0_10"

# -----------------------------------------------------------------------
# Folder locations -- may need to be changed for your environment
# -----------------------------------------------------------------------

APP_PATH="/tmp/filenet/samples/ce/demo"
CLIENT_JAR_PATH="/tmp/filenet/samples/ce"

# -----------------------------------------------------------------------
# Set the CLASSPATH and options, then run
# -----------------------------------------------------------------------

APP_JAR="${APP_PATH}/cesample.jar"
LOG4J_JAR="${CLIENT_JAR_PATH}/lib/log4j.jar"
JACE_JAR="${CLIENT_JAR_PATH}/lib/Jace.jar"
JBOSS_JAR="${CLIENT_JAR_PATH}/lib/jbossall-client.jar"

CLASSPATH="${APP_JAR}:${LOG4J_JAR}:${JACE_JAR}:${JBOSS_JAR}"

JAAS="-Djava.security.auth.login.config=${CLIENT_JAR_PATH}/config/jaas.conf.JBoss"
NAMING="-Djava.naming.factory.initial=org.jnp.interfaces.NamingContextFactory"

${JAVA_HOME}/bin/java -cp $CLASSPATH $JAAS $NAMING cesample.MainFrame
 
