#!/bin/sh

# -----------------------------------------------------------------------
# WebSphere appclient init -- may need to be changed for your environment
# -----------------------------------------------------------------------

# for remote development environment use following
WAS_CLIENT_DIR="/opt/IBM/WebSphere/AppClient"
JAVA_HOME="${WAS_CLIENT_DIR}/java"

# for local development environment use following
#WAS_SERVER_DIR="/opt/IBM/WebSphere/AppServer"
#JAVA_HOME="${WAS_SERVER_DIR}/java"

# -----------------------------------------------------------------------
# WebSphere sas.client.props file modification --
# Set "com.ibm.CORBA.loginSource" property to "none".
# It is locatted under ${WAS_CLIENT_DIR}/properties in case of websphere application client 
# and ${WAS_SERVER_DIR}/profiles/AppSrv01/properties in case of websphere application server
# -----------------------------------------------------------------------

# -----------------------------------------------------------------------
# Folder locations -- may need to be changed for your environment
# -----------------------------------------------------------------------

APP_PATH="/tmp/filenet/samples/ce/demo"
CLIENT_JAR_PATH="/tmp/filenet/samples/ce"

# -----------------------------------------------------------------------
# Set the CLASSPATH and options, then run
# -----------------------------------------------------------------------

APP_JAR="${APP_PATH}/cesample.jar"
LOG4J_JAR="${CLIENT_JAR_PATH}/lib/log4j.jar"
JACE_JAR="${CLIENT_JAR_PATH}/lib/Jace.jar"

CLASSPATH="${APP_JAR}:${LOG4J_JAR}:${JACE_JAR}"

JAAS="-Djava.security.auth.login.config=${CLIENT_JAR_PATH}/config/jaas.conf.WebSphere"
NAMING="-Djava.naming.factory.initial=com.ibm.websphere.naming.WsnInitialContextFactory"

# for remote development environment use following
EXTDIRS="-Djava.ext.dirs=${WAS_CLIENT_DIR}/java/jre/lib/ext:${WAS_CLIENT_DIR}/lib:${WAS_CLIENT_DIR}/plugins"
CLIENTSAS="-Dcom.ibm.CORBA.ConfigURL=file:${WAS_CLIENT_DIR}/properties/sas.client.props"

# for local development environment use following
#EXTDIRS="-Djava.ext.dirs=${WAS_SERVER_DIR}/java/jre/lib/ext:${WAS_SERVER_DIR}/runtimes"
#CLIENTSAS="-Dcom.ibm.CORBA.ConfigURL=file:${WAS_SERVER_DIR}/profiles/AppSrv01/properties/sas.client.props"

${JAVA_HOME}/bin/java -cp $CLASSPATH $CLIENTSAS $JAAS $NAMING $EXTDIRS cesample.MainFrame
