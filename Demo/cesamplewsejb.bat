@echo off

setlocal

rem -----------------------------------------------------------------------
rem WebSphere appclient init -- may need to be changed for your environment
rem -----------------------------------------------------------------------

rem for remote development environment use following
set WAS_CLIENT_DIR=C:\Program Files\IBM\WebSphere\AppClient
set JAVA_HOME=%WAS_CLIENT_DIR%\java

rem for local development environment use following
rem set WAS_SERVER_DIR=C:\Program Files\IBM\WebSphere\AppServer
rem set JAVA_HOME=%WAS_SERVER_DIR%\java

rem -----------------------------------------------------------------------
rem WebSphere sas.client.props file modification
rem Set "com.ibm.CORBA.loginSource" property to "none".
rem It is locatted under ${WAS_CLIENT_DIR}/properties in case of websphere application client
rem and ${WAS_SERVER_DIR}/profiles/AppSrv01/properties in case of websphere application server
rem -----------------------------------------------------------------------

rem -----------------------------------------------------------------------
rem Folder locations -- may need to be changed for your environment
rem -----------------------------------------------------------------------

set APP_PATH=C:\temp\filenet\samples\ce\demo
set CLIENT_JAR_PATH=C:\temp\filenet\samples\ce

rem -----------------------------------------------------------------------
rem Set the CLASSPATH and options, then run
rem -----------------------------------------------------------------------

set APP_JAR=%APP_PATH%\cesample.jar
set JACE_JAR=%CLIENT_JAR_PATH%\lib\Jace.jar
set LOG4J_JAR=%CLIENT_JAR_PATH%\lib\log4j.jar

set CLASSPATH=%APP_JAR%;%JACE_JAR%;%LOG4J_JAR%

set JAAS=-Djava.security.auth.login.config=%CLIENT_JAR_PATH%\config\jaas.conf.WebSphere
set NAMING=-Djava.naming.factory.initial=com.ibm.websphere.naming.WsnInitialContextFactory

rem for remote development environment use following
set EXTDIRS=-Djava.ext.dirs=%WAS_CLIENT_DIR%\java\jre\lib\ext;%WAS_CLIENT_DIR%\lib;%WAS_CLIENT_DIR%\plugins
set CLIENTSAS=-Dcom.ibm.CORBA.ConfigURL=file:%WAS_CLIENT_DIR%\properties\sas.client.props

rem for local development environment use following
rem set EXTDIRS=-Djava.ext.dirs=%WAS_SERVER_DIR%\java\jre\lib\ext;%WAS_SERVER_DIR%\runtimes
rem set CLIENTSAS=-Dcom.ibm.CORBA.ConfigURL=file:%WAS_SERVER_DIR%\profiles\AppSrv01\properties\sas.client.props

"%JAVA_HOME%\bin\java" -cp "%CLASSPATH%" "%CLIENTSAS%" "%JAAS%" "%NAMING%" "%EXTDIRS%" cesample.MainFrame
