#!/bin/sh

# -----------------------------------------------------------------------
# JRE location -- edit or remove (if already set) for your environment
# -----------------------------------------------------------------------

JAVA_HOME="/opt/jdk150_04"

# -----------------------------------------------------------------------
# Folder locations -- may need to be changed for your environment
# -----------------------------------------------------------------------

APP_PATH="/tmp/filenet/samples/ce/demo"
CLIENT_JAR_PATH="/tmp/filenet/samples/ce"

# -----------------------------------------------------------------------
# Set the CLASSPATH and options, then run
# -----------------------------------------------------------------------

APP_JAR="${APP_PATH}/cesample.jar"
LOG4J_JAR="${CLIENT_JAR_PATH}/lib/log4j.jar"
JACE_JAR="${CLIENT_JAR_PATH}/lib/Jace.jar"
WEBLOGIC_JAR="${CLIENT_JAR_PATH}/lib/weblogic.jar"
# for weblogic 10.0 and higher use wlfullclient5.jar instead of weblogic.jar
#WEBLOGIC_JAR="${CLIENT_JAR_PATH}/lib/wlfullclient5.jar"

CLASSPATH="${APP_JAR}:${LOG4J_JAR}:${JACE_JAR}:${WEBLOGIC_JAR}"

JAAS="-Djava.security.auth.login.config=${CLIENT_JAR_PATH}/config/jaas.conf.WebLogic"
NAMING="-Djava.naming.factory.initial=weblogic.jndi.WLInitialContextFactory"

${JAVA_HOME}/bin/java -cp $CLASSPATH $JAAS $NAMING cesample.MainFrame
