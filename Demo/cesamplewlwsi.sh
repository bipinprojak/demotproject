#!/bin/sh

# -----------------------------------------------------------------------
# JRE location -- edit or remove (if already set) for your environment
# -----------------------------------------------------------------------

JAVA_HOME="/opt/jdk150_04"

# -----------------------------------------------------------------------
# Folder locations -- may need to be changed for your environment
# -----------------------------------------------------------------------

APP_PATH="/tmp/filenet/samples/ce/demo"
CLIENT_JAR_PATH="/tmp/filenet/samples/ce"

# -----------------------------------------------------------------------
# Set the CLASSPATH and options, then run
# -----------------------------------------------------------------------

APP_JAR="${APP_PATH}/cesample.jar"
LOG4J_JAR="${CLIENT_JAR_PATH}/lib/log4j.jar"
JACE_JAR="${CLIENT_JAR_PATH}/lib/Jace.jar"
STAX_JAR="${CLIENT_JAR_PATH}/lib/stax-api.jar:${CLIENT_JAR_PATH}/lib/xlxpScanner.jar:${CLIENT_JAR_PATH}/lib/xlxpScannerUtils.jar"

CLASSPATH="${APP_JAR}:${LOG4J_JAR}:${JACE_JAR}:${STAX_JAR}"

${JAVA_HOME}/bin/java -cp $CLASSPATH cesample.MainFrame
