package p8.sample.weblogic;

//Import.


import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.Iterator;

import javax.activation.MimeType;
import javax.security.auth.Subject;

import org.json.JSONException;
import org.json.JSONObject;
import org.omg.CORBA.OBJECT_NOT_EXIST;

import cesample.CEUtil;

import com.filenet.api.admin.ClassDefinition;
import com.filenet.api.admin.DatabaseStorageArea;
import com.filenet.api.admin.LocalizedString;
import com.filenet.api.admin.PropertyDefinitionString;
import com.filenet.api.admin.PropertyTemplateInteger32;
import com.filenet.api.admin.PropertyTemplateString;
import com.filenet.api.admin.StorageArea;
import com.filenet.api.admin.StoragePolicy;
import com.filenet.api.collection.ContentElementList;
import com.filenet.api.collection.ObjectStoreSet;
import com.filenet.api.collection.PropertyDefinitionList;
import com.filenet.api.constants.AutoClassify;
import com.filenet.api.constants.AutoUniqueName;
import com.filenet.api.constants.Cardinality;
import com.filenet.api.constants.CheckinType;
import com.filenet.api.constants.ClassNames;
import com.filenet.api.constants.DefineSecurityParentage;
import com.filenet.api.constants.FilteredPropertyType;
import com.filenet.api.constants.PropertyNames;
import com.filenet.api.constants.RefreshMode;
import com.filenet.api.core.Connection;
import com.filenet.api.core.ContentReference;
import com.filenet.api.core.ContentTransfer;
import com.filenet.api.core.Document;
import com.filenet.api.core.Domain;
import com.filenet.api.core.Factory;
import com.filenet.api.core.Folder;
import com.filenet.api.core.ObjectStore;
import com.filenet.api.core.ReferentialContainmentRelationship;
import com.filenet.api.property.FilterElement;
import com.filenet.api.property.Properties;
import com.filenet.api.property.PropertyFilter;
import com.filenet.api.util.Id;
import com.filenet.api.util.UserContext;

public class P8Connector {
	 

	
	
	
	
	



	public void CreateDoc(ObjectStore objStore){
		
	
		   Document doc = Factory.Document.createInstance(objStore, ClassNames.DOCUMENT);
		      System.out.println("ANUPOS");        
		      doc.getProperties().putValue("DocumentTitle", "Docx_New");
		      doc.save(RefreshMode.NO_REFRESH);
		       
		      
		  /*      doc.checkin(AutoClassify.DO_NOT_AUTO_CLASSIFY, CheckinType.MAJOR_VERSION);
		        doc.save(RefreshMode.NO_REFRESH);*/
		      
		      
		        
		        Folder folder = Factory.Folder.getInstance(objStore, ClassNames.FOLDER, new Id("{0CEC00AF-A21B-4F06-80DF-E4CC62326B2B}")); // id of folder to which you want to store document.
		        ReferentialContainmentRelationship rcr = folder.file(doc, AutoUniqueName.AUTO_UNIQUE, "DemoDOCUMENT",
		                DefineSecurityParentage.DO_NOT_DEFINE_SECURITY_PARENTAGE);
		        rcr.save(RefreshMode.NO_REFRESH);
	       
		        System.out.println("done doc creation");
		
	}
	//Demo Comment for checking
	//done Change
    //Done again changes to check pull request in bitbucket ok ok ok
    //p8connector code ok ok okie
	
	
	public void AddContent(ObjectStore objStore)
	{
		
		
		// ************************************ Document Content Addition Code****************************
        
        // Get document.
            Document doc=Factory.Document.getInstance(objStore, ClassNames.DOCUMENT, "/Nitin/Desert.jpg" );

            // Check out the Document object and save it.
            doc.checkout(com.filenet.api.constants.ReservationType.EXCLUSIVE, null, doc.getClassName(), doc.getProperties());
            doc.save(RefreshMode.REFRESH);

            // Get the Reservation object from the Document object.
            Document reservation = (Document) doc.get_Reservation();

            // Specify internal and external files to be added as content.
            File internalFile = new File("C:\\Users\\Public\\Pictures\\Sample Pictures\\Koala.jpg");
            // non-Windows: File internalFile = new File("/tmp/docs/mydoc.txt");
         //   String externalFile = "text.txt";

            // Add content to the Reservation object.
            try {
                // First, add a ContentTransfer object.
                ContentTransfer ctObject = Factory.ContentTransfer.createInstance();
                FileInputStream fileIS = new FileInputStream(internalFile.getAbsolutePath());
                ContentElementList contentList = Factory.ContentTransfer.createList();
                ctObject.setCaptureSource(fileIS);
                // Add ContentTransfer object to list.
                contentList.add(ctObject);
                ctObject.set_ContentType("image/jpeg");

                reservation.set_ContentElements(contentList);
                reservation.save(RefreshMode.REFRESH);
                }
            catch (Exception e)
            {
                System.out.println(e.getMessage() );
            }

            // Check in Reservation object as major version.
            reservation.checkin(AutoClassify.DO_NOT_AUTO_CLASSIFY, CheckinType.MAJOR_VERSION);
            reservation.save(RefreshMode.REFRESH);
            
            System.out.println("Added Content");
	
	}
		
  public void GetProperty(ObjectStore objStore){
	
	
    
    //Getting document properties using fetch instacne        
  Document doc1=Factory.Document.fetchInstance(objStore,"{2B957E22-45ED-43D3-B98B-77B511121173}" ,null);     
         doc1.save(RefreshMode.REFRESH);
 
   //Changing the storage policy of document
       StoragePolicy sp = Factory.StoragePolicy.getInstance(objStore, new Id("{1EAAF460-E75A-4A56-8E1E-54DA62DAC849}") );
         doc1.set_StoragePolicy(sp);
         doc1.save(RefreshMode.REFRESH);
         
      
         
    //Converting to json object JSONObject obj = new JSONObject();
         JSONObject jobj=new JSONObject();
               try {
				jobj.put("Name", doc1.get_Name());
				jobj.put("Size", doc1.get_ContentSize());
				jobj.put("GUID", doc1.get_Id());
				jobj.put("Date", doc1.get_DateCreated());
				jobj.put("Creator", doc1.get_Creator());
				jobj.put("Policy", doc1.get_StoragePolicy().get_DisplayName());
	            jobj.put("Owner", doc1.get_Owner());
	            jobj.put("StorageArea", doc1.get_StorageArea().get_DisplayName());
	            jobj.put("VersionStatus", doc1.get_VersionStatus());
                
	            System.out.println(jobj);
                  
        doc1.set_Owner("psmall");
           doc1.save(RefreshMode.NO_REFRESH);
	
	
	
               } catch (Exception e) {
   				// TODO Auto-generated catch block
   				e.printStackTrace();
   			}
   	    
   	    finally
   	    {
   	       UserContext.get().popSubject();
   	    }
   	              
}	
	
	
 public void MoveContent(ObjectStore objStore )
{
	try{
	   // Get the storage area where you want to move the document content.
    StorageArea dsa = Factory.StorageArea.fetchInstance(objStore, new Id("{54B1ECD7-D9C1-48A3-8B96-81442BDC7276}"), null );

     // Get the Document object whose content you want to move.
     Document docx = Factory.Document.getInstance(objStore, ClassNames.DOCUMENT, "{2B757BEB-3798-42DB-8AE0-554520AD12D5}" );

     // Move the content and save the Document object.
     docx.moveContent(dsa);
     docx.save(RefreshMode.REFRESH);
     System.out.println("Moved the content");
	    } catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
 
 finally
 {
    UserContext.get().popSubject();
 }
 
}
	

public void ChangeTitle(ObjectStore objStore)

{
	   /*Document title changing code*/    
    
     
      PropertyFilter pf1 = new PropertyFilter();
	        pf1.addIncludeProperty(new FilterElement(null, null, null, "DocumentTitle", null));
	        Document doc5 = Factory.Document.fetchInstance(objStore, new Id("{2C0D07B0-C6E2-450A-A0AE-F31B6106AEA2}"),pf1 );

	        
	        Properties props = doc5.getProperties();
	        
	        props.putValue("DocumentTitle", "virus");

	        
	        doc5.save(RefreshMode.REFRESH );
	        
	        System.out.println("Updated");
   
}


public void FetchDocument(ObjectStore objStore)
{
	
 
	
	// Get document and populate property cache.
	PropertyFilter pf = new PropertyFilter();
	pf.addIncludeProperty(new FilterElement(null, null, null, PropertyNames.CONTENT_SIZE, null) );
	pf.addIncludeProperty(new FilterElement(null, null, null, PropertyNames.CONTENT_ELEMENTS, null) );
	Document doc=Factory.Document.fetchInstance(objStore, "	{8D422270-F78C-456C-90B1-1EB51A74B6B5}", null );
	        doc.save(RefreshMode.REFRESH);

	// Print information about content elements.
	System.out.println("No. of document content elements: " + doc.get_ContentElements().size() + "\n" +
	 "Total size of content: " + doc.get_ContentSize() + "\n");

	// Get content elements and iterate list.
	ContentElementList docContentList = doc.get_ContentElements();
	Iterator iter = docContentList.iterator();
	while (iter.hasNext() )
	{
	    ContentTransfer ct = (ContentTransfer) iter.next();

	    // Print element sequence number and content type of the element.
	    System.out.println("\nElement Sequence number: " + ct.get_ElementSequenceNumber().intValue() + "\n" +
	     "Content type: " + ct.get_ContentType() + "\n");

               
    	String fileName = doc.get_Name();
    	String M=doc.get_MimeType();
    	System.out.println(fileName);
    	
    
    	String MType=doc.get_MimeType();
    	
    		
        CEUtil.writeDocContentToFile(doc, "D:\\");
		

    	
	}


}

public void createDocumentClass(ObjectStore objStore,String strSubclassName) throws IOException{
	
	
	InputStreamReader converter = new InputStreamReader(System.in);
	BufferedReader in = new BufferedReader(converter);

	String strSymbolicName = "Document";

	
	ClassDefinition objClassDef = Factory.ClassDefinition.fetchInstance(objStore, strSymbolicName, null);
	   
	System.out.println("Class definition selected: " + objClassDef.get_Name());
	System.out.println("Type the name to assign to the new subclass: ");
	//String strSubclassName = in.readLine();

	
	ClassDefinition objClassDefNew = objClassDef.createSubclass();

	
	LocalizedString objLocStr = Factory.LocalizedString.createInstance();
	objLocStr.set_LocalizedText(strSubclassName);
	objLocStr.set_LocaleName(objStore.get_LocaleName());

	
	objClassDefNew.set_DisplayNames(Factory.LocalizedString.createList());
	objClassDefNew.get_DisplayNames().add(objLocStr);

	
	objClassDefNew.save(RefreshMode.REFRESH);            
	System.out.println("New class definition created: " + objClassDefNew.get_Name());       
	          
	
}

public void createPropertyTemplates(ObjectStore objStore, String propertyName)  { 

	
	//PropertyTemplateString newPropTemplate = Factory.PropertyTemplateString.createInstance(objStore); 
	PropertyTemplateInteger32 newPropTemplate = Factory.PropertyTemplateInteger32.createInstance(objStore); 

	// Set cardinality of properties that will be created from the property template 
	newPropTemplate.set_Cardinality (Cardinality.SINGLE); 

	// Set up locale 
	LocalizedString locStr = Factory.LocalizedString.createInstance(); 
	locStr.set_LocalizedText(propertyName); //NewishProperty
	locStr.set_LocaleName (objStore.get_LocaleName()); 

	// Create LocalizedString collection 
	newPropTemplate.set_DisplayNames (Factory.LocalizedString.createList()); 
	newPropTemplate.get_DisplayNames().add(locStr); 

	// Save new property template to the sever 
	newPropTemplate.save(RefreshMode.REFRESH); 
	System.out.println("Property Template "+ locStr  + "has been created"); 

	
	 
	} 

public void createCustomProperty(ObjectStore objStore,String className, String propertyName){ 
 
	PropertyFilter pf = new PropertyFilter(); 
	pf.addIncludeType(0, null, Boolean.TRUE, FilteredPropertyType.ANY); 
	
	ClassDefinition classDef = Factory.ClassDefinition.fetchInstance(objStore, className, pf); 

	PropertyTemplateString newPropTemplate = Factory.PropertyTemplateString.createInstance(objStore); 
 
	newPropTemplate.set_Cardinality (Cardinality.SINGLE); 

	
	LocalizedString locStr = Factory.LocalizedString.createInstance(); 
	locStr.set_LocalizedText(propertyName); 
	locStr.set_LocaleName (objStore.get_LocaleName()); 

	
	newPropTemplate.set_DisplayNames (Factory.LocalizedString.createList()); 
	newPropTemplate.get_DisplayNames().add(locStr); 

	
	newPropTemplate.save(RefreshMode.REFRESH); 
	System.out.println("Property Template" + locStr + "has been created"); 

	// Create property definition from property template 
	PropertyDefinitionString newPropDef = (PropertyDefinitionString)newPropTemplate.createClassProperty(); 

	// Get PropertyDefinitions property from the property cache 
	PropertyDefinitionList propDefs = classDef.get_PropertyDefinitions(); 

	// Add new property definition to class definition 
	propDefs.add(newPropDef); 
	classDef.save(RefreshMode.REFRESH); 
	System.out.println("Property Definition "+ newPropDef + " has been added to class "+ className); 
	} 

public void ChangeClass(ObjectStore objStore)
{
	 Document doc1=Factory.Document.fetchInstance(objStore,"{9D3AFD42-C195-4497-9967-A6BE980391A4}" ,null);     
	 doc1.changeClass("Employee");
      doc1.save(RefreshMode.REFRESH);
      System.out.println("Done");
      System.out.println(doc1.get_ClassDescription().get_DisplayName());
    


}

public static void main(String[] args)
{
    // Set connection parameters; substitute for the placeholders.
    String uri = "http://ecmdemo1:9080/wsi/FNCEWS40MTOM/";
    String username = "p8admin";
    String password = "filenet";
        
    // Make connection.
    Connection conn = Factory.Connection.getConnection(uri);
    Subject subject = UserContext.createSubject(conn, username, password, null);
    UserContext.get().pushSubject(subject);

    try
    {
       // Get default domain.
       Domain domain = Factory.Domain.fetchInstance(conn, null, null);
       System.out.println("Domain: " + domain.get_Name());

       ObjectStoreSet osColl = domain.get_ObjectStores();

         // Get particular object store.
	      ObjectStore objStore = Factory.ObjectStore.fetchInstance(domain, "ANUPOS", null); 
	    
     
	 P8Connector p=new P8Connector();
	// p.CreateDoc(objStore);
	 //p.ChangeTitle(objStore);       
	 //   p.GetProperty(objStore);
	// p.AddContent(objStore);
	 //   p.FetchDocument(objStore);
	// p.ChangeClass(objStore);
	 
	// p.createDocumentClass(objStore,"NewishDocument1");
	// p.createPropertyTemplates(objStore,"NewishProperty1");
	 p.createCustomProperty( objStore,"NewishDocument1","NewishProperty1");
	 
    }
    catch(Exception e){
    	
    	e.printStackTrace();
       }
	      
                 
  }
}


